########################################################################
## SCRIPT TO DELETE FALSE POSITIVES IN TC-TRACKING AND DYNAMIC OUTPUT ##
########################################################################
import pandas as pd
import glob
import os
import sys

path = sys.argv[1]
dyn  = sys.argv[2]
if len(sys.argv) > 3:
    input = int(sys.argv[3])
    assert input > 0 and 1080%input==0,"lifetime argument should be a positive divisor of 1080min (18h)"
    min_lifetime = 1080//input
else:
    min_lifetime = 3
    
tracks = glob.glob(path + "/traject_*.txt")

min_det = [0.1, 0.2, 0.5, 0.5, 0.5, 0.5, 0.5]

#delete false positive tracks
for track in tracks:
    detected = False
    df = pd.read_csv(track)
    det_perc = df[" det_per(curr)"]
    cat = df[" cat"]+1
    for id in df.index:
        if det_perc[id] >= min_det[cat[id]]:
            detected = True
    if len(df.index) > min_lifetime and detected:
        #set marker for dynamic output around detected TCs
        track_id = track[-10:-4]
        dyn_outs = glob.glob(path + "/NWP*"+track_id+"*.nc")
        for dyn_out in dyn_outs:
            os.rename(dyn_out,dyn_out+"det")
        pass
    else:
        os.remove(track)
        track_id = track[-10:-4]

if dyn == "dyn":
    #remove dynamic output around false positives
    dyn_outs = glob.glob(path + "/NWP*.nc")
    for dyn_out in dyn_outs:
        os.remove(dyn_out)

    #rename-back the dynamic output around detected TCs
    dyn_outs = glob.glob(path + "/NWP*.ncdet")
    for dyn_out in dyn_outs:
        os.rename(dyn_out,dyn_out[:-3])

