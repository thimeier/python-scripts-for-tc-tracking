###################################################
## SCRIPT TO PLOT TRANSLATIONAL VELOCITY OF A TC ##
###################################################
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
import pandas as pd
import glob
import os
import sys
from haversine import haversine, Unit

path = sys.argv[1]
trackid = sys.argv[2]
timestep = int(sys.argv[3])
df  = pd.read_csv(path + '/traject_'+trackid+'.txt')

dates = df[' DAT']
dts = np.linspace(timestep,len(dates[1:])*timestep,len(dates[1:]))
lons = df[' LON']*180/np.pi
lats = df[' LAT']*180/np.pi
trans_v = np.empty(len(dates[1:]))

for idx, date in enumerate(dates):
    if idx == 0:
        continue
    start = (lats[idx-1],lons[idx-1])
    end = (lats[idx],lons[idx])

    distance = haversine(start,end)
    speed = distance*1000/timestep/60
    trans_v[idx-1] = speed
        
print(trans_v)
plt.plot(dts,trans_v)
plt.title("Translational Velocity of TC "+trackid)
plt.xlabel("time [\u0394h]")
plt.ylabel("translational velocity [m/s]")
#plt.show()
plt.savefig(path+'/trans_v_'+str(trackid)+'.png', dpi=300)
