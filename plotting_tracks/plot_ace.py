#######################################
## SCRIPT TO PLOT ACE OF A TC season ##
#######################################
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
import pandas as pd
import glob
import os
import sys
from datetime import timedelta
from datetime import datetime

# Get path to ACE data
path = sys.argv[1]
aces = glob.glob(path + '/ACE*.txt')

# Read in ACE data
for ace_file in aces:
	df  = pd.read_csv(ace_file)
	ace = df.cumsum()
	ace_all = ace[" ACE_all"]
	ace_1pl = ace["ACE_CAT1plus"]
	ace_det = ace["ACE_detected"]
	
	# Define start date and number of entries
	start_date = datetime(year=2014,month=8, day=1)
	n_dts = len(ace_all)  
	
	# Generate array of dates
	dts = np.linspace(0, (n_dts - 1) * 6, n_dts, dtype='float') * 3600
	dts = start_date + np.array([timedelta(seconds=s) for s in dts])
	
	# Plot ACE (remove commented lines to plot comparing with the inclusion of short lived TCs)
	plt.rcParams.update({'font.size':18})
	fig, ax = plt.subplots(1, 1, figsize=(10, 8))
	#ax.plot(dts,ace_1pl, label="CAT 1+")
	ax.plot(dts,ace_det, label="perturbation"+ace_file[-5])
	#ax.plot(dts,ace_det, label="detected")
	#ax.plot(dts,ace_all, label="all")
	
plt.legend()
plt.title("ACE of TC season", fontweight='bold')
plt.xticks(rotation=20)
plt.ylabel("Accumulated CE  $[10^4 \cdot$ m$^2/$s$^2]$")
#plt.show()
plt.savefig(path+'/ACE_merge.png', dpi=300)
