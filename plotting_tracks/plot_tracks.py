##############################################
## SCRIPT TO PLOT ALL TRACKS OF A TC SEASON ##
##############################################

import matplotlib
import matplotlib.pyplot as plt
from  matplotlib import cm
import numpy as np
import xarray as xr
import cartopy.crs as ccrs
import cartopy.util as cutil
from matplotlib.patches import Rectangle
from matplotlib.collections import LineCollection
import matplotlib.ticker as ticker
import matplotlib.patheffects as pe
import pandas as pd
import glob
import os
import sys

# helper function for label formatting
def format_lon(x, pos):
    if x < 0:
        return f"{abs(x)}°W"
    else:
        return f"{x}°E"

def format_lat(x, pos):
    if x < 0:
        return f"{abs(x)}°S"
    else:
        return f"{x}°N"

# Input: PATH to trajectory files
path = sys.argv[1]
all_tracks = glob.glob(path + "/traject_*.txt")
print("tracks detected: ", len(all_tracks))

lon_bds = [-120, -20]
lat_bds = [10 , 60]

# setup figure
plt.rcParams.update({'font.size':16})
fig, ax = plt.subplots(1, 1, subplot_kw=dict(projection=ccrs.PlateCarree()), figsize=(10, 6))
ax.set_xlim(lon_bds[0]-2, lon_bds[1]+2)
ax.set_ylim(lat_bds[0]-2, lat_bds[1]+2)
colormap = np.array(["#6EC1EA","#4DFFFF","#FFFFD9","#FFD98C","#FF9E59","#FF738A","#A188FC"])
labels = np.array(["TD","TS","CAT 1","CAT 2","CAT 3","CAT 4","CAT 5"])
for i in range(7):
    ax.plot(0,0,c=colormap[i],label=labels[i],linewidth=3)
ax.legend(ncols=2,loc='upper left')

# set up ticks
xt = np.arange(lon_bds[0],lon_bds[1]+1,20)
yt = np.arange(lat_bds[0],lat_bds[1]+1,10)

ax.set_xticks(xt)
ax.set_yticks(yt)
ax.xaxis.set_major_formatter(ticker.FuncFormatter(format_lon))
ax.yaxis.set_major_formatter(ticker.FuncFormatter(format_lat))
ax.coastlines(linewidth=0.5)

# values and coordinates taken from search algorithm
for track in all_tracks:
    center_coords_on  = np.loadtxt(track, delimiter=',', skiprows=1, usecols=(1,2,6), dtype='float', ndmin=2)
    center_x = center_coords_on[:,0]*180/np.pi
    center_y = center_coords_on[:,1]*180/np.pi
    cat = center_coords_on[:,2].astype(int)

    # Create line segments
    points = np.array([center_x, center_y]).T.reshape(-1, 1, 2)
    segments = np.concatenate([points[:-1], points[1:]], axis=1)
    lc = LineCollection(segments, colors=colormap[cat[:-1]+1], linewidth=1.5, path_effects=[pe.Stroke(linewidth=1.9, foreground='black'), pe.Normal()])
    ax.add_collection(lc)

ax.set(title='TC tracks')
plt.savefig(path+'/tracks.png', dpi=1200)