import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xarray as xr
from pathlib import Path
from matplotlib.patches import Rectangle
from matplotlib.colors import BoundaryNorm
import matplotlib.ticker as ticker
import sys
import os
import glob
from haversine import haversine_vector, Unit

# ------------
# credit: https://stackoverflow.com/questions/25983218/scientific-notation-colorbar user: "unutbu"
def fmt(x, pos):
    a, b = '{:.2e}'.format(x).split('e')
    b = int(b)
    return r'${} \times 10^{{{}}}$'.format(a, b)
# ------------

plt.rcParams.update({'font.size':18})

lon_bds = [-130, -5]
lat_bds = [-10, 80]

path = sys.argv[1]
trackid = sys.argv[2]
dynamic = sys.argv[3]
prefix = sys.argv[4]
p_type = int(sys.argv[5]) # 1: vertical velocity, 2: radial velocity, 3: horizontal velocity

Path(path+'/plots').mkdir(parents=True, exist_ok=True)

if prefix == "-":
    h_coords = "HL_V_DOM01_"

if p_type == 3:
  cmap = 'YlGnBu'
else:
  cmap = 'bwr'

# if trackid is given as 0 -> plot all tracks
if trackid == "0":
    all_tracks = glob.glob(path + "/traject_*.txt")
else:
    all_tracks = glob.glob(path + '/traject_'+trackid+'.txt')

for track in all_tracks:
    trackid = track[-10:-4]
    print("plotting track: "+trackid)

    # generate folder to store plots of trajectory
    Path(path+'/plots/'+trackid).mkdir(parents=True, exist_ok=True)  
  
    df  = pd.read_csv(path + '/traject_'+trackid+'.txt')
    max_cat = max(df[' cat'])
    dts = df[' DAT']
    
    # define grid-points 
    r = np.array([0,5,15,25,40,60,80,100,150,200,300,400,500],dtype="float") #try higher resolution
    midpt_r = 0.5*(r[:-1] + r[1:])
    d = np.arange(-5,5.2,0.2)
    
    n_r = r.size
    n_d = d.size
  
    for idx, row in df.iterrows():
        date = row[' DAT']
        print(date)
        month = date[5:7]
        day = date[8:10]
        hour = date[11:13]
        if dynamic == 'dyn':
            filename = path + '/NWP_LAM_' + h_coords + trackid + '_2005' + month + day + 'T' + hour + '0000Z.nc'
        else:
            filename = path + '/NWP_LAM_' + h_coords + '0001'  + '_2005' + month + day + 'T' + hour + '0000Z.nc'
      
        if os.path.isfile(filename)==False:
            continue
      
        fig, ax = plt.subplots(1, 1, figsize=(10, 8))
        data = xr.open_dataset(filename)
      
        center_x = row[' LON']*180/np.pi
        center_y = row[' LAT']*180/np.pi
                
        cent_tup = (center_y,center_x)
        print(cent_tup)
      
        # compute r-axis elements
        n_cells = data.ncells.values[-1]+1
        n_lev = data.alt.values.size
        lons = data.clon*180/np.pi
        lats = data.clat*180/np.pi
        cell_coords = list(zip(lats,lons))
        cent_coords = list(n_cells * (cent_tup,))
        radius = haversine_vector(cent_coords, cell_coords, Unit.KILOMETERS)
        print(np.argmin(radius))
        
        # get z-axis elements (pressure coordinates / height coordinates)
        height = data.alt.values
        
        # Compute the central wind components
        u_cnt = data.u.values[0,:,np.argmin(radius)]
        v_cnt = data.v.values[0,:,np.argmin(radius)]
        
        # Compute the relative wind components
        u_rel = data.u.values[0,:,:] - u_cnt[:, np.newaxis]
        v_rel = data.v.values[0,:,:] - v_cnt[:, np.newaxis]
        
        # set up prs-radius matrix
        if True:
          val = np.zeros((n_lev,n_r-1))
          cnt = np.zeros((n_lev,n_r-1))
          for i in range(0,n_r-1):
            mask = (radius >= r[i]) & (radius < r[i + 1])
            for j in range(0,n_lev):              
              if p_type == 1:     # PLOT VERTICAL WIND
                val[j,i] += data.w.values[0,j,mask].sum()
              
              elif p_type == 2:   # PLOT RADIAL/TANGENTIAL WIND
                dx = lons[mask] - center_x
                dy = lats[mask] - center_y
                dr = np.sqrt(dx**2 + dy**2)
                u_r = (u_rel[j, mask] * dx + v_rel[j, mask] * dy) / dr
                u_t = (u_rel[j, mask] * dy + v_rel[j, mask] * dx) / dr
                val[j,i] += u_r.sum()
              
              elif p_type == 3:   # PLOT HORIZONTAL WIND
                uv = np.sqrt(v_rel[j,mask]**2 + u_rel[j,mask]**2)
                val[j,i] += uv.sum()
              
              else:
                pass
      
              cnt[j,i] += mask.sum()
              if cnt[j,i] == 0:
                print("no entries in bucket: " ,r[i], " to " ,r[i+1])
          val = val/cnt           
                
        if p_type == 2:
          vmin = -15
          vmax =  15
          levels = np.arange(vmin, vmax+1, 1)
          cbar_ticks = np.arange(vmin, vmax+1, 5)
        elif p_type == 1:
          vmin = -1
          vmax =  1
          levels = np.linspace(vmin, vmax, 21)
          cbar_ticks = np.linspace(vmin, vmax, 11)
        else:
          vmin =  0
          vmax = 50
          levels = np.arange(vmin, vmax+1, 5)
          cbar_ticks = np.arange(vmin, vmax+1, 10)
          
          
        h = ax.contourf(midpt_r, height*0.001, val, cmap=cmap, levels=levels)
                        
        cbar = fig.colorbar(h, extend='both')
        cbar.set_ticks(cbar_ticks)
              
        ax.set(title='track at: 2005-' +month+'-'+day+'T'+hour+'h' )
        
        if p_type == 1:
          fig.suptitle('Plot of vertical wind [ms${^{-1}}$] along tc track', fontweight='bold')
        elif p_type == 2:
          fig.suptitle('Plot of radial wind [ms${^{-1}}$] along tc track', fontweight='bold')
        else:
          fig.suptitle('Plot of horizontal wind [${ms^{-1}}$] along tc track', fontweight='bold')
          
        ax.xaxis.labelpad = 15
        ax.yaxis.labelpad = 15
        
        fig.text(0.5, 0.02, 'radius from centre [km]', ha='center')
        fig.text(0.02, 0.5, 'height [km]', va='center', rotation='vertical')
      
        #fig.tight_layout()
        #plt.show()
        fields = ["w", "v_r", "uv"]
        plt.savefig(path+'/plots/'+trackid+'/'+fields[p_type-1]+'_'+month + day + 'T' + hour+'.png', dpi=500)