#######################################################
## SCRIPT TO PLOT TWO TRAJECTORIES ONTOP EACH OTHER  ##
## WITH THEIR UNDERLYING PRS/TEM/VOR/... FIELD       ##
#######################################################
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xarray as xr
import cartopy.crs as ccrs
import cartopy.util as cutil
from pathlib import Path
from matplotlib.patches import Rectangle
from matplotlib.colors import BoundaryNorm
import matplotlib.ticker as ticker
import glob
import sys
import os

# ------------
# credit: https://stackoverflow.com/questions/25983218/scientific-notation-colorbar user: "unutbu"
def fmt(x, pos):
    a, b = '{:.2e}'.format(x).split('e')
    b = int(b)
    return r'${} \times 10^{{{}}}$'.format(a, b)
# ------------

plt.rcParams.update({'font.size':18})

def format_lon(x, pos):
    if x < 0:
        return f"{abs(x):.1f}°W"
    else:
        return f"{x:.1f}°E"

def format_lat(x, pos):
    if x < 0:
        return f"{abs(x):.1f}°S"
    else:
        return f"{x:.1f}°N"

lon_bds = [-130, -5]
lat_bds = [-10, 80]

# Input:  1) PATH to data, 2&3) 6-digit traject IDs, 4) prs/tem/vor/... 
#         5) dyn/stat depending on output type of ICON [, 6) Prefix of ICON output files ]
path = sys.argv[1]
trackid  = sys.argv[2]
trackid2 = sys.argv[3]
field = sys.argv[4]
dynamic = sys.argv[5]

Path(path+'/plots').mkdir(parents=True, exist_ok=True)

# Default file prefixes (if no 5-th argument)
if field == "prs":
    h_coords = "DOM01_"
    unit = "hPa"
elif field == "tem":
    h_coords = "PL_T_DOM01_"
    unit = "K"
else:
    h_coords = "PL_DOM01_"
    unit = "1/s"

# Custom file prefix (5-th argument)
if len(sys.argv) > 6:
    prefix = sys.argv[6]
else:
    prefix = "NWP_LAM_"+h_coords


# values and coords from output
if field == "prs":
    cmap = 'YlGnBu'
else:
    cmap = 'bwr'

# if trackid is given as 0 -> plot all tracks
if trackid == "0":
    all_tracks = glob.glob(path + "/traject_*.txt")
else:
    all_tracks = glob.glob(path + '/traject_'+trackid+'.txt')

for track in all_tracks:
    trackid = track[-10:-4]
    
    print("plotting track: "+trackid)
    
    df  = pd.read_csv(track)
    centers_x = df[' LON'].to_numpy()*180/np.pi
    centers_y = df[' LAT'].to_numpy()*180/np.pi
    max_cat = max(df[' cat'])
    
    df2 = pd.read_csv(path + '/traject_'+trackid2+'.txt')
    centers_x2 = df2[' LON'].to_numpy()*180/np.pi
    centers_y2 = df2[' LAT'].to_numpy()*180/np.pi
    
    # generate folder to store plots of trajectory
    Path(path+'/plots/'+trackid).mkdir(parents=True, exist_ok=True)

    for idx, row in df.iterrows():
        date = row[' DAT']
        print(date)
        month = date[5:7]
        day = date[8:10]
        hour = date[11:13]
        if dynamic == 'dyn':
            filename = path + '/' + prefix + trackid + '_2005' + month + day + 'T' + hour + '0000Z.nc'
        else:
            filename = path + '/' + prefix + '000001'+ '_2005' + month + day + 'T' + hour + '0000Z.nc'
    
        if os.path.isfile(filename)==False:
            print("no file found, skipping to next step")
            continue
    
        # setup of figure
        fig, ax = plt.subplots(1, 1, subplot_kw=dict(projection=ccrs.PlateCarree()), figsize=(10, 8))
        data = xr.open_dataset(filename)
    
        center_x = row[' LON']*180/np.pi
        center_y = row[' LAT']*180/np.pi
    
        lons = data.clon*180/np.pi
        lats = data.clat*180/np.pi
    
        lon_bds = [center_x-5.2, center_x+5.2]
        lat_bds = [center_y-5.2, center_y+5.2]
    
        xt = [center_x-5, center_x, center_x+5]
        yt = [center_y-5, center_y, center_y+5]
    
        ax.set_xlim(lon_bds[0], lon_bds[1])
        ax.set_ylim(lat_bds[0], lat_bds[1])
    
        ax.set_xticks(xt)
        ax.set_yticks(yt)
        ax.xaxis.set_major_formatter(ticker.FuncFormatter(format_lon))
        ax.yaxis.set_major_formatter(ticker.FuncFormatter(format_lat))
        ax.coastlines(linewidth=0.5)
    
        # set appropriate values depending on field and intensity of TC
        if field == "prs":
            values = data.pres_msl.values[0,:] * 0.01
            vmin = 1000
            vmax = 1020
            if max_cat > 1:
                vmin = 1000 - max_cat * 20
        elif field == "tem":
            values = data.temp.values[0,0,:]
            m_lon = abs(lons-center_x) < 5.2
            m_lat = abs(lats-center_y) < 5.2
            m_val = values > 1
            mask = m_lon & m_lat & m_val
            tmean = (values[mask]).mean()
            values = values - tmean
            values[~mask] = 0
            vmin = - 5 - max_cat
            vmax = + 5 + max_cat
        else:
            values = data.vor.values[0,3,:]
            vmin = -2.5e-3
            vmax =  2.5e-3

        # Create colormap and ticks       
        if field == "tem":
            levels = np.arange(vmin, vmax+1)
            cbar_ticks = np.arange(vmin, vmax+1, 2)
        elif field == "prs":
            levels = np.arange(vmin, vmax+1, 5)
            cbar_ticks = np.arange(vmin, vmax+1, 10)
        else:
            levels = np.linspace(vmin, vmax, 21)
            cbar_ticks = np.linspace(vmin, vmax, 11)

        # Do plotting (Field)    
        h = ax.tricontourf(data.clon*180/np.pi, data.clat*180/np.pi, values,
                           transform=ccrs.PlateCarree(), cmap=cmap, levels=levels)                           
        if field == "vor":
            cbar = fig.colorbar(h, format=ticker.FuncFormatter(fmt), extend='both')
        else:
            cbar = fig.colorbar(h, extend='both')
            
        cbar.set_ticks(cbar_ticks) 
    
        # Do plotting (Trajectories)
        ax.plot(centers_x,centers_y,color='purple')
        ax.scatter(center_x,center_y,color='purple')
        ax.plot(centers_x2,centers_y2,color='orange')
        ax.scatter(centers_x2[idx],centers_y2[idx],color='orange')
    
        ax.set(title='track at: 2005-' +month+'-'+day+'T'+hour+'h' )
    
        if field == "tem":
            fig.suptitle('Plot of '+ field +' anomaly [Δ'+ unit +'] along tc track', fontweight='bold')
        else:
            fig.suptitle('Plot of '+ field +' field [' + unit +'] along tc track', fontweight='bold')
    
        fig.tight_layout()
        #plt.show()
        
        plt.savefig(path+'/plots/'+trackid+'/'+field+'_'+month + day + 'T' + hour+'.png', dpi=300)
