import xarray as xr
import numpy as np
import math

def havdist(lon1,lon2,lat1,lat2):
    # Radius of the Earth in kilometers
    R = 6371.0
    
    # Differences in coordinates
    dlon = lon2 - lon1
    dlat = lat2 - lat1

    # Haversine formula
    a = np.sin(dlat / 2)**2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon / 2)**2
    c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a))

    # Distance
    d = R * c

    return d

# testing on all variables used for tracking (could be extended to other variables stored
# on grid cell centers). clon,clat and z_mc are to show correct grid info is taken.
# The remaining ones show that the values are also selected correctly.
vars = ['clon','clat','vor','pres_msl','z_mc','u','v','temp']

times = ['00']

errors = np.zeros((np.size(times), np.size(vars)))

for i in range(np.size(times)):
    file_stat = '../sample_data/staticNWP_LAM_DOM01_0001_20050801T000320Z.nc'
    file_dyn = '../sample_data/dynamicNWP_LAM_DOM01_0024_20050801T000320Z.nc'

    data_dyn  = xr.open_dataset(file_dyn )
    data_stat = xr.open_dataset(file_stat)

    #print(data_dyn.variables)
    #print(data_stat.variables)
    
    clomax = data_dyn.clon.values.max()
    clamax = data_dyn.clat.values.max()
    clomin = data_dyn.clon.values.min()
    clamin = data_dyn.clat.values.min()
    cntr_lon = -1.85479#(clomax + clomin)/2.0
    cntr_lat =  0.41842#(clamax + clamin)/2.0
    n_cells = np.size(data_dyn.clon.values)

    # used to compare "square" output
    mask = ((data_stat.clat.values >= clamin) & (data_stat.clat.values <= clamax) & (data_stat.clon.values >= clomin) & (data_stat.clon.values <= clomax))
    
    # used to compare "circle" output (more cumbersome)
    mask = (havdist(cntr_lon,data_stat.clon.values,cntr_lat,data_stat.clat.values) <= 499.95)
    mask2= (havdist(cntr_lon,data_dyn.clon.values,cntr_lat,data_dyn.clat.values) <= 500)

    j = 0
    # relative root mean square error calculation
    for var_i in vars:
        var_s = getattr(data_stat, var_i)
        var_d = getattr(data_dyn , var_i)
        relrmsqerr = np.linalg.norm(var_s.isel(ncells=mask).values - var_d.values)/np.linalg.norm(var_s.isel(ncells=mask).values)
        errors[i,j] = relrmsqerr
        j +=1
j = 0
for var_i in vars:
    print(var_i, ',' ,errors[:,j].mean())
    j += 1


