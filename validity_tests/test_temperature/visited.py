import matplotlib.pyplot as plt
import matplotlib
import numpy as np

font = {
        'weight' : 'bold',
        'size'   : 22}
        
matplotlib.rc('font', **font)

data = np.loadtxt('visited_0009_coord.txt', dtype='float')    #data for border point
data_2 = np.loadtxt('visited_0005_coord.txt', dtype='float')
data_3 = np.loadtxt('visited_0006_coord.txt', dtype='float')
data_4 = np.loadtxt('visited_0008_coord.txt', dtype='float')

data_bdry = np.loadtxt('visited_0000_coord.txt', dtype='float') #data for boundary point

fig, ax = plt.subplots()

x = data[:,0]
y = data[:,1]
ax.scatter(x, y, marker="1", label="PE: 9 (home)", color="tab:green")

x = data_2[:,0]
y = data_2[:,1]
ax.scatter(x, y, marker="+", label="PE: 5", color="tab:red")

x = data_3[:,0]
y = data_3[:,1]
ax.scatter(x, y, marker="x", label="PE: 6", color="tab:blue")

x = data_4[:,0]
y = data_4[:,1]
ax.scatter(x, y, marker="2", label="PE: 8", color="tab:orange")

x = data_bdry[:,0]
y = data_bdry[:,1]
#ax.scatter(x, y, marker="1", label="PE: 0 (home)", color="tab:purple")


ax.scatter(-1.749836, 0.7587970, color = 'black', label="center")    #border point
#ax.scatter(-2.099452, 0.096019, color = 'black', label="center")  #boundary point

ax.set_xlabel('longitude in radian')
ax.set_ylabel('latitude in radian')
ax.legend(loc='upper right')
ax.set_title("Centers of visited cells during DFS - Boundary Case")

plt.show()

