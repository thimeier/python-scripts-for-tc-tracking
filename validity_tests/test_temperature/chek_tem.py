import numpy as np
import matplotlib.pyplot as plt

data_tem  = np.loadtxt('visited_0006_val.txt', dtype='float')  #border case
data_tem2 = np.loadtxt('visited_0005_val.txt', dtype='float')
data_tem3 = np.loadtxt('visited_0008_val.txt', dtype='float')
data_tem4 = np.loadtxt('visited_0009_val.txt', dtype='float')

data_tem0 = np.loadtxt('visited_0000_val.txt', dtype='float') #boundary case

temp_arr = np.concatenate((data_tem[:,0], data_tem2[:,0], data_tem3[:,0], data_tem4[:,0]))
#temp_arr = data_tem0[:,0]
dist_arr = np.concatenate((data_tem[:,2], data_tem2[:,2], data_tem3[:,2], data_tem4[:,2]))
#dist_arr = data_tem0[:,2]

print(np.mean(temp_arr))
print(np.mean(temp_arr[dist_arr<400000]))
print(np.mean(temp_arr[dist_arr<300000]))
print(np.mean(temp_arr[dist_arr<200000]))
print(np.mean(temp_arr[dist_arr<100000]))
print(np.mean(temp_arr[dist_arr< 50000]))
print(temp_arr[dist_arr<1])
