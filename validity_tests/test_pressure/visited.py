import matplotlib.pyplot as plt
import numpy as np

data   = np.loadtxt('vistprs_0006_coord.txt', dtype='float')    #data for border point
data_2 = np.loadtxt('vistprs_0005_coord.txt', dtype='float')
data_3 = np.loadtxt('vistprs_0001_coord.txt', dtype='float')

data_bdry = np.loadtxt('vistprs_0000_coord.txt', dtype='float') #data for boundary point

fig, ax = plt.subplots()

x = data[:,0]
y = data[:,1]
ax.scatter(x, y, marker="1", label="PE: 6 (home)", color="tab:blue")

x = data_2[:,0]
y = data_2[:,1]
ax.scatter(x, y, marker="+", label="PE: 5", color="tab:red")

x = data_3[:,0]
y = data_3[:,1]
ax.scatter(x, y, marker="x", label="PE: 1", color="gold")

x = data_bdry[:,0]
y = data_bdry[:,1]
#ax.scatter(x, y,marker="1", label="PE: 0 (home)", color="tab:purple")

ax.scatter(-1.80629, 0.446656, color = 'black', label="center")    #border point
#ax.scatter(-2.099452157646360, 9.60185741742995E-02, color = 'black', label="center")  #boundary point

ax.set_xlabel('longitude in radian')
ax.set_ylabel('latitude in radian')
ax.legend(loc='upper right')
ax.set_title("Centers of visited cells during DFS - Border Case")
plt.show()

