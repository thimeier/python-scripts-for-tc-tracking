import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np
from tkinter import *
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg, NavigationToolbar2Tk)
# Implement the default Matplotlib key bindings.
from matplotlib.backend_bases import key_press_handler
import xarray as xr
import cartopy.crs as ccrs
import cartopy.util as cutil
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.patches import Rectangle

# TODO: fill in path to output file
file = 'TODO'
data = xr.open_dataset(file)


# setup
clon = data.clon.values
clat = data.clat.values
_size = np.size(data.clat.clon)
voc = np.arange(0, 3*_size).reshape((_size,3))
vlons = data.clon_bnds.values.flatten()
vlats = data.clat_bnds.values.flatten()
max_lon = clon.max()*180/np.pi
min_lon = clon.min()*180/np.pi
max_lat = clat.max()*180/np.pi
min_lat = clat.min()*180/np.pi
midpt_lon = (max_lon + min_lon)/2.
midpt_lat = (max_lat + min_lat)/2.
cmap = 'YlGnBu'
fig, ax = plt.subplots(1, 1, subplot_kw=dict(projection=ccrs.PlateCarree()))

# uncomment for custom plot bounds
#lon_bds = [-180,180]
#lat_bds = [-90,90]
#ax.set_xlim(lon_bds[0], lon_bds[1])
#ax.set_ylim(lat_bds[0], lat_bds[1])

# different set of ticks (choose one)
xt = [min_lon, midpt_lon, max_lon]
yt = [min_lat, midpt_lat, max_lat]
#xt = np.linspace(lon_bds[0],lon_bds[1],721)
#yt = np.linspace(lat_bds[0],lat_bds[1],361)

ax.set_xticks(xt)
ax.set_yticks(yt)
ax.set_xlabel('longitude in deg')
ax.set_ylabel('latitude in deg')
ax.coastlines(linewidth=0.5)

# three possible plots for mslp: 1) grid cells, 2) contour, 3) contour with tctrack data
#h = ax.tripcolor(vlons*180/np.pi, vlats*180/np.pi, voc, data.pres_msl.values[0,:], transform=ccrs.PlateCarree(), cmap=cmap, edgecolors='k')
h = ax.tricontourf(data.clon*180/np.pi, data.clat*180/np.pi, data.pres_msl.values[0,:], transform=ccrs.PlateCarree(), cmap=cmap, edgecolors='k')

#ax.add_patch(Rectangle((-106.75, 23.5), 1, 1, edgecolor = 'red', fill=False, lw=1))
             
# add colourbar
cbar = plt.colorbar(h)

ax.set(title='Local mslp min. at '+file[-19:-3])
plt.show()


