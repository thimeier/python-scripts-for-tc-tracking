import pandas as pd
import math
import glob
import os
import sys

path_in  = sys.argv[1]
path_out = sys.argv[2]
df       = pd.read_csv(path_in)

df['lon'] = df['lon'].apply(math.radians) 
df['lat'] = df['lat'].apply(math.radians)

output_dir = path_out+'/sliced' 
os.makedirs(output_dir, exist_ok=True)

for tc_id, group in df.groupby('tc_id'): 
    group = group.rename(columns={'date': ' DAT'})
    group = group.rename(columns={'lon': ' LON'})
    group = group.rename(columns={'lat': ' LAT'})
    group = group.rename(columns={'curr_cat': ' cat'})
    filename = os.path.join(output_dir, f'{tc_id:06}.txt')
    group[[' DAT', ' LON', ' LAT', 'slp', 'maxwind', 'percent_count',' cat']].to_csv(filename, index=False)
